#As much a reminder of how it was done as to use as an actual script
#Probably needs to be executed from the home folder of the pi

pip install PySimpleGUI tqdm requests bs4


git init
git remote add -f origin https://gitlab.developers.cam.ac.uk/met-teaching-office/log_in.git
git pull origin main

# should end up with the repo files in the RPi home folder, I suppose it doesn't need to be there, w/e
# worth checking that the log_in.py script is executable too, I think it should be

sudo ln -s /home/$(whoami)/log_in.py /usr/local/bin/log_in

sudo echo "@log_in" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart

#doing it this way allows the creation of a desktop icon for anyone to use to restart the program

### Set up screen saver, comment all this out if you don't want it

sudo apt install xscreensaver xscreensaver-gl xscreensaver-gl-extra xscreensaver-data-extra -y

cd /home/$(whoami)/
git clone https://gitlab.developers.cam.ac.uk/met-teaching-office/get_doitpoms_micrographs.git

line="0 0 * * * python /home/msm-class/get_doitpoms_micrographs/get_micrographs.py"
(crontab -u $(whoami) -l; echo "$line" ) | crontab -u $(whoami) -

